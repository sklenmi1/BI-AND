package cz.cvut.sklenmi1.screens.configure;

import static cz.cvut.sklenmi1.data.DbConstants.*;

import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

import cz.cvut.sklenmi1.R;
import cz.cvut.sklenmi1.data.FeedReaderContentProvider;
import cz.cvut.sklenmi1.data.SyncReceiver;
import cz.cvut.sklenmi1.screens.dialogs.AddFeedDialog;
import cz.cvut.sklenmi1.screens.dialogs.DeleteFeedDialog;


/**
 * A simple {@link Fragment} subclass.
 */
public class ConfigureFeedsFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final int FEED_LOADER = 1;

    public ConfigureFeedsFragment() {
        // Required empty public constructor
    }

    private FRAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().getActionBar().setTitle(R.string.configure_feeds);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setEmptyText(getString(R.string.configure_feed_empty_text));
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(FEED_LOADER, null, this);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        DeleteFeedDialog.show(getFragmentManager(), id);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.configure_feeds, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_feed_add:
                AddFeedDialog.show(getFragmentManager());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case FEED_LOADER:
                return new CursorLoader(getActivity(), FeedReaderContentProvider.FEED_CONTENT_URI, null, null, null, null);
            default:
                break;
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        switch (loader.getId()) {
            case FEED_LOADER:
                if (mAdapter == null) {
                    mAdapter = new FRAdapter(getActivity(), data);
                    setListAdapter(mAdapter);
                } else {
                    mAdapter.swapCursor(data);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case FEED_LOADER:
                mAdapter.swapCursor(null);
            default:
                break;
        }
    }

    private static class FRAdapter extends CursorAdapter {
        private final Context mContext;
        private LayoutInflater mInflater;

        private FRAdapter(Context context, Cursor cursor) {
            super(context, cursor, false);
            mContext = context;
            mInflater = LayoutInflater.from(context);
        }

        private static class ViewHolder {
            TextView title;
            TextView url;

            ViewHolder(View view) {
                title = (TextView) view.findViewById(R.id.feed_entry_title);
                url = (TextView) view.findViewById(R.id.feed_entry_url);
            }
        }


        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View view = mInflater.inflate(R.layout.configure_feeds_feed, parent, false);
            ViewHolder holder = new ViewHolder(view);
            view.setTag(holder);
            return view;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            ViewHolder holder = (ViewHolder) view.getTag();
            String feedTitle = cursor.getString(cursor.getColumnIndex(TITLE));
            holder.title.setText(TextUtils.isEmpty(feedTitle) ?
                    context.getText(R.string.configure_feeds_unknown_feed_title) : feedTitle);
            holder.url.setText(cursor.getString(cursor.getColumnIndex(URL_LINK)));
        }
    }

}
