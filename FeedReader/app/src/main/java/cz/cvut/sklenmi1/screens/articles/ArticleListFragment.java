package cz.cvut.sklenmi1.screens.articles;

import static cz.cvut.sklenmi1.data.DbConstants.*;

import android.app.Fragment;
import android.app.ListFragment;
import android.app.LoaderManager;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.TextView;

import cz.cvut.sklenmi1.R;
import cz.cvut.sklenmi1.data.FeedReaderContentProvider;

/**
 * Created by michal.sklenar on 14/03/16.
 */
public class ArticleListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {
    private static final int ARTICLE_LOADER = 1;
    private int mCurrentPosition = -1;
    private boolean wideLayout = false;

    public ArticleListFragment() {
        // Required empty public constructor
    }

    private ArticleAdapter mAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setEmptyText(getString(R.string.articles_empty_text));
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getLoaderManager().initLoader(ARTICLE_LOADER, null, this);

    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Cursor cursor = (Cursor) mAdapter.getItem(position);
        long articleId = cursor.getLong(cursor.getColumnIndex(ID));
        Bundle b = new Bundle();
        b.putLong(ArticleDetailFragment.ARTICLE_ID, articleId);

        mCurrentPosition = position;

        if (wideLayout) {
            Fragment f = new ArticleDetailFragment();
            f.setArguments(b);
            getFragmentManager().beginTransaction().replace(R.id.fragment2, f).commit();
            mAdapter.setPosition(position);
            mAdapter.notifyDataSetChanged();
        } else {
            Intent intent = new Intent(getActivity(), ArticleDetailActivity.class);
            intent.putExtras(b);
            startActivity(intent);
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        switch (id) {
            case ARTICLE_LOADER:
                return new CursorLoader(getActivity(), FeedReaderContentProvider.ARTICLE_CONTENT_URI, null, null, null, null);
            default:
                break;
        }
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        wideLayout = getActivity().findViewById(R.id.fragment2) != null;
        switch (loader.getId()) {
            case ARTICLE_LOADER:
                if (mAdapter == null) {
                    mAdapter = new ArticleAdapter(getActivity(), data, mCurrentPosition, wideLayout);
                    setListAdapter(mAdapter);
                } else {
                    mAdapter.swapCursor(data);
                    mAdapter.setPosition(mCurrentPosition);
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        switch (loader.getId()) {
            case ARTICLE_LOADER:
                mAdapter.swapCursor(null);
            default:
                break;
        }
    }

    private static class ArticleAdapter extends CursorAdapter {
        private final Context mContext;
        private LayoutInflater mInflater;
        private int mSelectedPosition;
        private boolean mWideLayout;

        private ArticleAdapter(Context context, Cursor cursor, int position, boolean wideLayout) {
            super(context, cursor, false);
            mContext = context;
            mInflater = LayoutInflater.from(context);
            mSelectedPosition = position;
            mWideLayout = wideLayout;
        }

        public void setPosition(int mCurrentPosition) {
            mSelectedPosition = mCurrentPosition;
        }

        private static class ViewHolder {
            TextView title;
            TextView perex;

            ViewHolder(View view) {
                title = (TextView) view.findViewById(R.id.title);
                perex = (TextView) view.findViewById(R.id.perex);
            }
        }

        @Override
        public View newView(Context context, Cursor cursor, ViewGroup parent) {
            View view = mInflater.inflate(R.layout.feed_view, parent, false);
            ViewHolder holder = new ViewHolder(view);
            view.setTag(holder);
            return view;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = super.getView(position, convertView, parent);
            if (mSelectedPosition == position && mWideLayout) {
                convertView.setBackgroundColor(Color.parseColor("#A8DFF4"));
            } else {
                convertView.setBackgroundColor(0);
            }
            return convertView;
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            ViewHolder holder = (ViewHolder) view.getTag();
            holder.title.setText(cursor.getString(cursor.getColumnIndex(TITLE)));
            holder.perex.setText(Html.fromHtml(cursor.getString(cursor.getColumnIndex(DESC))));
        }
    }
}