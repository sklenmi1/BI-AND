package cz.cvut.sklenmi1.screens.articles;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import cz.cvut.sklenmi1.R;
import cz.cvut.sklenmi1.data.DownloadService;
import cz.cvut.sklenmi1.data.ScheduleBroadcastReceiver;
import cz.cvut.sklenmi1.data.SyncReceiver;
import cz.cvut.sklenmi1.screens.configure.ConfigureFeedsActivity;

/**
 * Created by michal.sklenar on 12/03/16.
 */
public class ArticleListActivity extends Activity implements DownloadService.ServiceCallbacks {

    private SyncReceiver mReceiver;
    private boolean isRunning = false;
    private DownloadService mService;
    boolean mBound = false;
    private MenuItem mRefreshItem;
    private View mProgressActionView;
    public static final String RUNNING ="running";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_list);
        mProgressActionView = LayoutInflater.from(this).inflate(R.layout.refresh_view_progress, null);
        mReceiver = new SyncReceiver();
        if (savedInstanceState != null) {
            isRunning = savedInstanceState.getBoolean(RUNNING);
        }
    }

    @Override
    protected void onResume() {
        registerReceiver(mReceiver, new IntentFilter(Intent.ACTION_RUN));
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (mReceiver != null) {
            unregisterReceiver(mReceiver);
        }
        super.onPause();
    }


    @Override
    protected void onStart() {
        super.onStart();
        Intent intent = new Intent(ScheduleBroadcastReceiver.SCHEDULE);
        sendBroadcast(intent);

        Intent bindIntent =  new Intent(this, DownloadService.class);
        getApplicationContext().bindService(bindIntent, mConnection, Context.BIND_AUTO_CREATE);
    }


    @Override
    protected void onStop() {
        super.onStop();
        if (mBound) {
            mService.setmCallbacks(null);
            getApplicationContext().unbindService(mConnection);
            mBound = false;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(RUNNING, isRunning);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.article_list_menu, menu);
        mRefreshItem = menu.findItem(R.id.action_refresh);

        if (isRunning || (mService != null && mService.isRunning())) {
            mRefreshItem.setActionView(mProgressActionView);
        } else {
            mRefreshItem.setActionView(null);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_config:
                intent = new Intent(this, ConfigureFeedsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_refresh:
                DownloadService.start(this);
                return true;
            case R.id.action_preferences:
                Log.e(ArticleDetailFragment.class.getName(), "preferences selected");
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /** Defines callbacks for service binding, passed to bindService() */
    private ServiceConnection mConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            DownloadService.LocalBinder binder = (DownloadService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;
            mService.setmCallbacks(ArticleListActivity.this);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    public void updateProgress() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mService.isRunning()) {
                    mRefreshItem.setActionView(mProgressActionView);
                    isRunning = true;
                } else {
                    mRefreshItem.setActionView(null);
                    isRunning = false;
                }
            }
        });
    }
}
