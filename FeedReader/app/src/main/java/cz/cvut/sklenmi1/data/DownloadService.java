package cz.cvut.sklenmi1.data;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Binder;
import android.os.IBinder;
import android.os.SystemClock;
import android.text.TextUtils;

import com.google.code.rome.android.repackaged.com.sun.syndication.feed.synd.SyndContent;
import com.google.code.rome.android.repackaged.com.sun.syndication.feed.synd.SyndEntry;
import com.google.code.rome.android.repackaged.com.sun.syndication.feed.synd.SyndFeed;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.FeedException;
import com.google.code.rome.android.repackaged.com.sun.syndication.io.SyndFeedInput;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import static cz.cvut.sklenmi1.data.DbConstants.AUTHOR;
import static cz.cvut.sklenmi1.data.DbConstants.CONTENT;
import static cz.cvut.sklenmi1.data.DbConstants.DESC;
import static cz.cvut.sklenmi1.data.DbConstants.FEED_ID;
import static cz.cvut.sklenmi1.data.DbConstants.ID;
import static cz.cvut.sklenmi1.data.DbConstants.PUB_DATE;
import static cz.cvut.sklenmi1.data.DbConstants.TITLE;
import static cz.cvut.sklenmi1.data.DbConstants.URL_LINK;

/**
 * Created by michal.sklenar on 13/05/16.
 */
public class DownloadService extends WakefulIntentService {
    public static boolean isLoading = false;
    public static final String BROADCAST ="feed_reader_progress_broadcast";
    private final IBinder mBinder = new LocalBinder();
    private ServiceCallbacks mCallbacks;


    public static interface ServiceCallbacks {
        void updateProgress();
    }

    public DownloadService() {
        super("DownloadService");
    }

    public void setmCallbacks(ServiceCallbacks callbacks) {
        mCallbacks = callbacks;
    }

    @Override
    protected void doWork(Intent intent) {
        isLoading = true;
        if (mCallbacks != null) {
            mCallbacks.updateProgress();
        }
        Cursor cursor = getContentResolver().query(FeedReaderContentProvider.FEED_CONTENT_URI, null, null, null, null);
        FeedReaderProviderHelper helper = new FeedReaderProviderHelper(getContentResolver());

        while (cursor.moveToNext()) {
            URL url = null;
            try {
                String urlLink = cursor.getString(cursor.getColumnIndex(URL_LINK));
                if (!urlLink.startsWith("http://") && !urlLink.startsWith("https://")) {
                    urlLink = "http://" + urlLink;
                }

                url = new URL(urlLink);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            SyndFeedInput input = new SyndFeedInput();
            try {
                if (url != null) {
                    SyndFeed feed = input.build(new InputStreamReader(url.openStream()));
                    if (TextUtils.isEmpty(cursor.getString(cursor.getColumnIndex(TITLE)))) {
                        ContentValues fValues = new ContentValues();
                        fValues.put(TITLE, feed.getTitle());
                        helper.updateFeedTitle(cursor.getLong(cursor.getColumnIndex(ID)), fValues);
                    }

                    List entries = feed.getEntries();
                    for (int i = 0; i < entries.size(); i++) {
                        SyndEntry entry = (SyndEntry) entries.get(i);
                        ContentValues values = new ContentValues();
                        values.put(TITLE, entry.getTitle());
                        values.put(URL_LINK, entry.getLink());
                        values.put(AUTHOR, entry.getAuthor());
                        if (entry.getPublishedDate() != null) {
                            values.put(PUB_DATE, String.valueOf(entry.getPublishedDate().getTime()));
                        }
                        values.put(FEED_ID, cursor.getString(cursor.getColumnIndex(ID)));
                        String desc;
                        if (entry.getDescription() != null) {
                            desc = entry.getDescription().getValue();
                        } else if (entry.getContents().size() > 0) {
                            desc = ((SyndContent) entry.getContents().get(0)).getValue();
                        } else {
                            desc = "";
                        }
                        values.put(DESC, desc);

                        String content;
                        if (entry.getContents().size() > 0) {
                            content = ((SyndContent) entry.getContents().get(0)).getValue();
                        } else if (entry.getDescription() != null) {
                            content = entry.getDescription().getValue();
                        } else {
                            content = "";
                        }
                        values.put(CONTENT, content);
                        helper.saveArticles(values);
                    }
                }
            } catch (FeedException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        cursor.close();
        for (int i = 0; i < 5; i++) {
            SystemClock.sleep(1000);
        }

        isLoading = false;
        if (mCallbacks != null) {
            mCallbacks.updateProgress();
        }
    }

    public static void start(Context gContext) {
        DownloadService.acquireStaticLock(gContext); //acquire a partial WakeLock
        gContext.startService(new Intent(gContext, DownloadService.class));
    }

    public class LocalBinder extends Binder {
        public DownloadService getService() {
            return DownloadService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public boolean isRunning() {
        return isLoading;
    }
}
