package cz.cvut.sklenmi1.screens.articles;

import static cz.cvut.sklenmi1.data.DbConstants.*;
import android.app.Fragment;
import android.app.LoaderManager;
import android.content.ContentUris;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import cz.cvut.sklenmi1.R;
import cz.cvut.sklenmi1.data.FeedReaderContentProvider;


/**
 * A simple {@link Fragment} subclass.
 */
public class ArticleDetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private Cursor mCursor;
    private TextView mTitle;
    private TextView mContent;
    private TextView mLink;
    private TextView mDate;

    public static final String ARTICLE_ID = "article_id";

    public ArticleDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().getActionBar().setTitle(R.string.article_detail_title);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle b = getArguments() == null ? getActivity().getIntent().getExtras() : getArguments();
        getLoaderManager().initLoader(1, b, this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_article_detail, container, false);
        mTitle = (TextView) view.findViewById(R.id.article_title);
        mContent = (TextView) view.findViewById(R.id.article_content);
        mDate = (TextView) view.findViewById(R.id.article_date_author);
        mLink = (TextView) view.findViewById(R.id.article_link);
        mLink.setMovementMethod(LinkMovementMethod.getInstance());
        mLink.setClickable(true);

        if (container != null) {
            container.removeView(container.findViewById(R.id.empty));
        }

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_share) {
            shareAction();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.article_detail_menu, menu);
    }

    private void shareAction() {
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, mCursor.getString(mCursor.getColumnIndex(TITLE)));
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, mCursor.getString(mCursor.getColumnIndex(URL_LINK)));
        startActivity(Intent.createChooser(sharingIntent, getString(R.string.share_label)));
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        long articleId = (long) args.get(ARTICLE_ID);
        return new CursorLoader(getActivity(), ContentUris.withAppendedId(FeedReaderContentProvider.ARTICLE_CONTENT_URI, articleId),
                null, null, null, null);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mCursor = data;
        if (data != null && mCursor.moveToFirst()) {
            mTitle.setText(Html.fromHtml(mCursor.getString(mCursor.getColumnIndex(TITLE))));
            String author = mCursor.getString(mCursor.getColumnIndex(AUTHOR));
            if (TextUtils.isEmpty(author)) {
                author = getString(R.string.unknown_author);
            }
            long date = mCursor.getLong(mCursor.getColumnIndex(PUB_DATE));
            if (date != 0) {
                CharSequence relativeDate = DateUtils.getRelativeTimeSpanString(date);
                mDate.setText(getString(R.string.article_date_author, relativeDate, author));
            } else {
                mDate.setText(getString(R.string.article_date_author, "", author).trim());
            }

            String linkText = "<a href='" + mCursor.getString(mCursor.getColumnIndex(URL_LINK)) + "'>" + getString(R.string.full_article) + "</a>";
            mLink.setText(Html.fromHtml(linkText));

            String contentString = data.getString(data.getColumnIndex(CONTENT));
            contentString = contentString.replaceAll("(?s)<style[^>]*>.*?</style>", "");
            contentString = contentString.replaceAll("(?s)<img.*?/>", "");
            contentString = contentString.replaceAll("(?s)<a[^>]*>\\s*</a>", "");
            contentString = contentString.replaceAll("(?s)<ul[^>]*>(.*?)</ul>", "<p>$1</p>");
            contentString = contentString.replaceAll("(?s)<li[^>]*>(.*?)</li>", "<br/>&bull; $1");
            contentString = contentString.replaceAll("(?s)(<br.*?/>\\s*)+", "<br/>");
            contentString = contentString.replaceAll("Read the Full Article Here", "");
            contentString = contentString.trim();
            mContent.setText(Html.fromHtml(contentString));
        }
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
    }
}
