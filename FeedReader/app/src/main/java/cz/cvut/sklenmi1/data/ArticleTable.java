package cz.cvut.sklenmi1.data;
import android.database.sqlite.SQLiteDatabase;

import static cz.cvut.sklenmi1.data.DbConstants.*;
/**
 * Created by michal.sklenar on 07/04/16.
 */
public class ArticleTable {
    public static final String TABLE_ARTICLE = "articleTable";

    private static final String DATABASE_CREATE = "create table "
            + TABLE_ARTICLE
            + "("
            + ID + " integer primary key autoincrement, "
            + TITLE + " text not null, "
            + URL_LINK + " text not null, "
            + DESC + " text not null, "
            + AUTHOR + " text not null, "
            + CONTENT + " text not null, "
            + PUB_DATE + " intteger null, "
            + FEED_ID + " integer not null"
            + ");";

    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVerison) {
        dropAndCreateTable(db);
    }

    public static void dropAndCreateTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTICLE);
        onCreate(db);
    }
}
