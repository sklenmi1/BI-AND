package cz.cvut.sklenmi1.data;

import static cz.cvut.sklenmi1.data.DbConstants.*;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;

/**
 * Created by michal.sklenar on 08/04/16.
 */
public class FeedReaderProviderHelper {
    private ContentResolver mResolver;

    public FeedReaderProviderHelper(ContentResolver mResolver) {
        this.mResolver = mResolver;
    }

    public void saveFeed(ContentValues values) {
        String feedSelection = URL_LINK + " = ?";
        String[] feedSelectionArgs = {values.getAsString(URL_LINK)};
        Cursor oldFeed = mResolver.query(FeedReaderContentProvider.FEED_CONTENT_URI, null, feedSelection, feedSelectionArgs, null);
        if (oldFeed != null && oldFeed.moveToFirst()) {
            long feedId = oldFeed.getLong(oldFeed.getColumnIndex(ID));
            mResolver.update(ContentUris.withAppendedId(FeedReaderContentProvider.ARTICLE_CONTENT_URI, feedId), values, null, null);
        } else {
            mResolver.insert(FeedReaderContentProvider.FEED_CONTENT_URI, values);
        }
        if (oldFeed != null) {
            oldFeed.close();
        }
    }

    public void saveArticles(ContentValues values) {
        String articleSelectin = URL_LINK + " = ?";
        String [] articleSelectionArgs = {values.getAsString(URL_LINK)};
        Cursor oldArticle = mResolver.query(FeedReaderContentProvider.ARTICLE_CONTENT_URI, null, articleSelectin, articleSelectionArgs, null);
        if (oldArticle != null && oldArticle.moveToFirst()) {
            long articleId = oldArticle.getLong(oldArticle.getColumnIndex(ID));
            mResolver.update(ContentUris.withAppendedId(FeedReaderContentProvider.ARTICLE_CONTENT_URI, articleId), values, null, null);
        } else {
            mResolver.insert(FeedReaderContentProvider.ARTICLE_CONTENT_URI, values);
        }

        if (oldArticle != null) {
            oldArticle.close();
        }
    }

    public void deleteFeed(long feedId) {
        String articleSelection = FEED_ID + " = ?";
        String[] articleSelectionArgs = {String.valueOf(feedId)};
        mResolver.delete(FeedReaderContentProvider.ARTICLE_CONTENT_URI, articleSelection, articleSelectionArgs);
        mResolver.delete(ContentUris.withAppendedId(FeedReaderContentProvider.FEED_CONTENT_URI, feedId), null, null);
    }

    public void updateFeedTitle(long feedId, ContentValues fValues) {
        mResolver.update(ContentUris.withAppendedId(FeedReaderContentProvider.FEED_CONTENT_URI, feedId), fValues, null, null);
    }
}
