package cz.cvut.sklenmi1.screens.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import cz.cvut.sklenmi1.R;
import cz.cvut.sklenmi1.data.FeedReaderContentProvider;
import cz.cvut.sklenmi1.data.FeedReaderProviderHelper;
import cz.cvut.sklenmi1.data.SyncReceiver;

import static cz.cvut.sklenmi1.data.DbConstants.URL_LINK;

/**
 * Created by michal.sklenar on 05/04/16.
 */
public class AddFeedDialog extends DialogFragment {
    private EditText feedUrlInput;

    public static void show(FragmentManager fragmentManager) {
        String tag = AddFeedDialog.class.getCanonicalName();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        Fragment existingDialog = fragmentManager.findFragmentByTag(tag);
        if (existingDialog != null) {
            transaction.remove(existingDialog);
        }

        AddFeedDialog newDialog = new AddFeedDialog();
        newDialog.show(transaction, tag);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.dialog_add_feed, null);
        feedUrlInput = (EditText) view.findViewById(R.id.add_feed_url);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.add_feed_dialog_title);
        builder.setView(view);

        builder.setPositiveButton(R.string.add_feed_dialog_add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String feedUrl = feedUrlInput.getText().toString();
                if (!feedUrl.isEmpty()) {
                    FeedReaderProviderHelper helper = new FeedReaderProviderHelper(getActivity().getContentResolver());
                    ContentValues values = new ContentValues();
                    values.put(URL_LINK, feedUrl);
                    helper.saveFeed(values);
                    Toast.makeText(getActivity(), R.string.add_feed_dialog_added, Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(SyncReceiver.RUN);
                    getActivity().sendBroadcast(i);
                } else {
                    Toast.makeText(getActivity(), R.string.add_feed_dialog_url_empty, Toast.LENGTH_SHORT).show();
                }
            }
        });

        builder.setNegativeButton(R.string.feed_dialogs_cancel, null);
        builder.setCancelable(true);

        return builder.create();
    }


}
