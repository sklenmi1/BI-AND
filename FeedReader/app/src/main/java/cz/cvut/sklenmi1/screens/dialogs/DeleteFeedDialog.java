package cz.cvut.sklenmi1.screens.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import cz.cvut.sklenmi1.R;
import cz.cvut.sklenmi1.data.FeedReaderContentProvider;
import cz.cvut.sklenmi1.data.FeedReaderProviderHelper;

/**
 * Created by michal.sklenar on 06/04/16.
 */
public class DeleteFeedDialog extends DialogFragment {
    private static final String ARG_FEED_ID = "feed_id";
    private long mFeedId;

    public static void show(FragmentManager fragmentManager, long feedId) {
        String tag = DeleteFeedDialog.class.getCanonicalName();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        Fragment existingDialog = fragmentManager.findFragmentByTag(tag);
        if (existingDialog != null) {
            transaction.remove(existingDialog);
        }

        // Create and show new dialog.
        DeleteFeedDialog newDialog = new DeleteFeedDialog();
        Bundle args = new Bundle();
        args.putLong(ARG_FEED_ID, feedId);
        newDialog.setArguments(args);
        newDialog.show(transaction, tag);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFeedId = getArguments().getLong(ARG_FEED_ID);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.delete_feed_dialog_title);
        builder.setMessage(R.string.delete_feed_dialog_delete_text);

        builder.setPositiveButton(R.string.delete_feed_dialog_delete, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                FeedReaderProviderHelper helper = new FeedReaderProviderHelper(getActivity().getContentResolver());
                helper.deleteFeed(mFeedId);
                Toast.makeText(getActivity(), R.string.delete_feed_dialog_deleted, Toast.LENGTH_SHORT).show();
            }
        });

        builder.setNegativeButton(R.string.feed_dialogs_cancel, null);
        builder.setCancelable(true);

        return builder.create();
    }
}
