package cz.cvut.sklenmi1.data;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class SyncReceiver extends BroadcastReceiver {
    public static final String RUN = "cz.cvut.sklenmi1.RUN";

    public SyncReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        DownloadService.start(context);
    }
}
