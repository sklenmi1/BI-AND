package cz.cvut.sklenmi1.data;

import static cz.cvut.sklenmi1.data.DbConstants.*;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;

/**
 * Created by michal.sklenar on 05/04/16.
 */
public class FeedReaderContentProvider extends ContentProvider {
    private FeedReaderDatabaseHelper dbHelper;

    public static final String AUTHORITY = "cz.cvut.sklenmi1";

    private static final int FEED_LIST = 1;
    private static final int FEED_ITEM = 2;
    private static final int ARTICLE_LIST = 3;
    private static final int ARTICLE_ITEM = 4;

    private static final String FEED_BASE_PATH = "feeds";
    private static final String ARTICLE_BASE_PATH = "articles";
    public static final Uri FEED_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + FEED_BASE_PATH);
    public static final Uri ARTICLE_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + ARTICLE_BASE_PATH);


    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sURIMatcher.addURI(AUTHORITY, FEED_BASE_PATH, FEED_LIST);
        sURIMatcher.addURI(AUTHORITY, FEED_BASE_PATH + "/#", FEED_ITEM);
        sURIMatcher.addURI(AUTHORITY, ARTICLE_BASE_PATH, ARTICLE_LIST);
        sURIMatcher.addURI(AUTHORITY, ARTICLE_BASE_PATH + "/#", ARTICLE_ITEM);
    }

    @Override
    public boolean onCreate() {
        dbHelper = new FeedReaderDatabaseHelper(getContext());
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();

        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case FEED_LIST:
                qb.setTables(FeedTable.TABLE_FEED);
                break;
            case ARTICLE_LIST:
                qb.setTables(ArticleTable.TABLE_ARTICLE);
                break;
            case ARTICLE_ITEM:
                qb.setTables(ArticleTable.TABLE_ARTICLE);
                String idSel = ID + " = " + ContentUris.parseId(uri);
                selection = TextUtils.isEmpty(selection) ? idSel : "(" + selection + ") AND " + idSel;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = qb.query(db, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = dbHelper.getWritableDatabase();
        long id = 0;
        String basePath = null;
        switch (uriType) {
            case FEED_LIST:
                id = sqlDB.insert(FeedTable.TABLE_FEED, null, values);
                basePath = FEED_BASE_PATH;
                break;
            case ARTICLE_LIST:
                id = sqlDB.insert(ArticleTable.TABLE_ARTICLE, null, values);
                basePath = ARTICLE_BASE_PATH;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(basePath + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = dbHelper.getWritableDatabase();
        int rowsDeleted = 0;
        switch (uriType) {
            case ARTICLE_LIST:
                rowsDeleted = sqlDB.delete(ArticleTable.TABLE_ARTICLE, selection, selectionArgs);
                break;
            case FEED_ITEM:
                String id = uri.getLastPathSegment();
                rowsDeleted = sqlDB.delete(FeedTable.TABLE_FEED, ID + "=" + id, null);
                break;
            case ARTICLE_ITEM:
                String articleID = uri.getLastPathSegment();
                rowsDeleted = sqlDB.delete(ArticleTable.TABLE_ARTICLE, ID + "=" + articleID, null);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        SQLiteDatabase sqlDB = dbHelper.getWritableDatabase();
        String idSelection = null;
        int rowsUpdated = 0;
        switch (uriType) {
            case FEED_ITEM:
                idSelection = ID + " = " + ContentUris.parseId(uri);
                selection = TextUtils.isEmpty(selection) ? idSelection : "(" + selection + ") AND " + idSelection;
                rowsUpdated = sqlDB.update(FeedTable.TABLE_FEED, values, selection, selectionArgs);
                break;
            case ARTICLE_ITEM:
                idSelection = ID + " = " + ContentUris.parseId(uri);
                selection = TextUtils.isEmpty(selection) ? idSelection : "(" + selection + ") AND " + idSelection;
                rowsUpdated = sqlDB.update(ArticleTable.TABLE_ARTICLE, values, selection, selectionArgs);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUpdated;
    }
}
