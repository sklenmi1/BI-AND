package cz.cvut.sklenmi1.data;

import static cz.cvut.sklenmi1.data.DbConstants.*;
import android.database.sqlite.SQLiteDatabase;
/**
 * Created by michal.sklenar on 05/04/16.
 */
public class FeedTable {

    public static final String TABLE_FEED = "feedTable";

    private static final String DATABASE_CREATE = "create table "
            + TABLE_FEED
            + "("
            + ID + " integer primary key autoincrement, "
            + TITLE + " text null, "
            + URL_LINK + " text not null "
            + ");";

    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(DATABASE_CREATE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        dropAndCreateTable(db);
    }

    public static void dropAndCreateTable(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FEED);
        onCreate(db);
    }
}
