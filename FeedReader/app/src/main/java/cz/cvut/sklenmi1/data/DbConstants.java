package cz.cvut.sklenmi1.data;

/**
 * Created by michal.sklenar on 05/04/16.
 */
public class DbConstants {

    private DbConstants() {
        throw new AssertionError();
    }

    public static final String ID = "_id";
    public static final String TITLE = "title";
    public static final String URL_LINK = "url";
    public static final String PUB_DATE = "publicationDate";
    public static final String FEED_ID = "feedId";
    public static final String AUTHOR = "author";
    public static final String CONTENT = "content";
    public static final String DESC = "description";
}
