package cz.cvut.sklenmi1.data;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

/**
 * Created by michal.sklenar on 15/05/16.
 */
public class ScheduleBroadcastReceiver extends BroadcastReceiver {
    public static final String SCHEDULE ="cz.cvut.sklenmi1.SCHEDULE";
    private static final long DOWNLOAD_INTERVAL = AlarmManager.INTERVAL_HOUR;//AlarmManager.INTERVAL_FIFTEEN_MINUTES / 15;


    @Override
    public void onReceive(Context context, Intent intent) {
        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent feedIntent = new Intent(SyncReceiver.RUN);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, feedIntent, PendingIntent.FLAG_NO_CREATE);

        if (pendingIntent != null) {
            alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime() + DOWNLOAD_INTERVAL,
                    DOWNLOAD_INTERVAL, pendingIntent);
        }
    }
}
