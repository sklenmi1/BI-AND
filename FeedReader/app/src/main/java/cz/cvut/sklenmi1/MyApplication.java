package cz.cvut.sklenmi1;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.SystemClock;

import cz.cvut.sklenmi1.data.DownloadService;
import cz.cvut.sklenmi1.data.SyncReceiver;

/**
 * Created by michal.sklenar on 15/05/16.
 */
public class MyApplication extends Application {
    private static final long DOWNLOAD_INTERVAL = AlarmManager.INTERVAL_HOUR;//AlarmManager.INTERVAL_FIFTEEN_MINUTES / 15;


    @Override
    public void onCreate() {
        super.onCreate();
        startRepeatingSynchronization();
    }

    public void startRepeatingSynchronization() {
        DownloadService.start(this);

        AlarmManager alarmManager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Intent feedIntent = new Intent(SyncReceiver.RUN);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, feedIntent, PendingIntent.FLAG_NO_CREATE);

        if (pendingIntent != null) {
            alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME,
                    SystemClock.elapsedRealtime() + DOWNLOAD_INTERVAL,
                    DOWNLOAD_INTERVAL, pendingIntent);
        }
    }
}
