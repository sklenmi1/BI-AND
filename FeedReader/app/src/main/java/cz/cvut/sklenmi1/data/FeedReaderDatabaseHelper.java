package cz.cvut.sklenmi1.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by michal.sklenar on 05/04/16.
 */
public class FeedReaderDatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "feedreader.db";
    public static final int DATABASE_VERSION = 1;

    public FeedReaderDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        FeedTable.onCreate(db);
        ArticleTable.onCreate(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        FeedTable.onUpgrade(db, oldVersion, newVersion);
        ArticleTable.onUpgrade(db, oldVersion, newVersion);
    }
}
